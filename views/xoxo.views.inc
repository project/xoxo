<?php

/**
 * Implementation of hook_views_data().
 */
function xoxo_views_data() {
  $data = array();

  foreach (oox_registry_get('factory')->getItemCache() as $type => $item) {
    // Declare a new base table.
    $table = $item['table'];
    $data[$table]['table'] = array(
      'group' => t("XoXo"),
      'base' => array(
        'field' => 'oid',
        'title' => t($item['name']),
        'help' => t("@name objects listing", array('@name' => $item['name'])),
      ),
    );

    // Then declare common fields. If any specific implementation want to
    // expose new fields, then they'll have to alter this definition.
    $data[$table]['oid'] = array(
      'title' => t("Unique identifier"),
      'help' => t("Unique internal integer identifier"),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      // FIXME: Create our own argument (that provides validation).
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
        'numeric' => TRUE,
      ),
      'filter' => array('handler' => 'views_handler_filter_numeric'),
      'sort' => array('handler' => 'views_handler_sort'),
    );
    // UID is copy from the node table definition.
    $data[$table]['uid'] = array(
      'title' => t('User'),
      'help' => t('User that owns or created this object'),
      'relationship' => array(
        'handler' => 'views_handler_relationship',
        'base' => 'users',
        'base field' => 'uid',
        'label' => t('user'),
      ),
    );
    // Status is copy from the node table definition.
    $data[$table]['status'] = array(
      'title' => t('Status'),
      'help' => t('Whether or not the the object is enabled'),
      'field' => array(
        'handler' => 'views_handler_field_boolean',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_boolean_operator',
        'label' => t('Published'),
        'type' => 'yes-no',
      ),
      'sort' => array('handler' => 'views_handler_sort'),
    );
    // Created is copy from the node table definition.
    $data[$table]['created'] = array(
      'title' => t('Creation date'),
      'help' => t('The date the node was posted.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array('handler' => 'views_handler_sort_date'),
      'filter' => array('handler' => 'views_handler_filter_date'),
    );
    // Updated is copy from the node table definition field changed.
    $data[$table]['updated'] = array(
      'title' => t('Updated date'),
      'help' => t('The date the node was last updated.'),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array('handler' => 'views_handler_sort_date'),
      'filter' => array('handler' => 'views_handler_filter_date'),
    );
    // FIXME: ability to extract description, there is chances that we will
    // be forced to load the object, this could be bad for performances, let
    // see what we'll find out.

    // Exportable specific stuff.
    if ($item['exportable']) {
      $data[$table]['name'] = array(
        'title' => t('Machine name'),
        'help' => t('Internal object unique identifier'), 
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        // Information for accepting a nid as an argument
        'argument' => array(
          'handler' => 'views_handler_argument_string',
          'numeric' => FALSE,
        ),
        // Information for accepting a nid as a filter
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        // Information for sorting on a nid.
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      );
      // FIXME: defaults can't be seen by views, maybe by altering the
      // results? But I don't think this can be possible.
    }

    // FIXME: If administrable, expose links to admin section.
  }

  return $data;
}

/**
 * Implementation of hook_views_plugins().
 */
function xoxo_views_plugins() {
  $data = array();
  $data['module'] = 'xoxo';
  $data['row'] = array();

  foreach (oox_registry_get('factory')->getItemCache() as $type => $item) {
    if ($item['renderable']) {
      $table = $item['table'];
      $data['row'][$table] = array(
        'title' => t($item['name']),
        'help' => t("@name display", array('@name' => $item['name'])),
        'handler' => 'views_plugin_row_xoxo_object_view',
        'path' => drupal_get_path('module', 'xoxo') . '/views/handlers',
        'theme' => 'xoxo_object_views_row',
        'theme path' => drupal_get_path('module', 'xoxo') . '/views/theme',
        'base' => array($table),
        'uses options' => TRUE,
        'type' => 'normal',
      );
    }
  }
  
  return $data;
}

function template_preprocess_xoxo_object_views_row($variables = array()) {
  $options = $variables['options'];
  $oid = (isset($variables['row']) && is_object($variables['row'])) ? $variables['row']->oid : NULL;
  if ($oid) {
    $table = $variables['view']->base_table;
    if ($type = oox_object_get_type_by_table($table)) {
      try {
        $object = xoxo_factory_get($type)->loadByIdentifier($oid);
        $variables['object'] = theme('xoxo_object', $object, $options['preview']);
      }
      catch (OoxException $e) { } 
    }
  }
}
