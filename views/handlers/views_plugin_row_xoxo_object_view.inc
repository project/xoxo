<?php

/**
 * @file
 * Contains the integration object view row style plugin.
 */

class views_plugin_row_xoxo_object_view extends views_plugin_row
{
  // Basic properties that let the row style follow relationships.
  var $base_table;
  var $base_field = 'oid';

  function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);
    $this->base_table = $view->base_table;
    if ($this->options['preview']) {
      $this->options['preview'] = (bool) $this->options['preview'];
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['preview'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['build_mode'] = array(
      '#type' => 'select',
      '#options' => array(
        0 => t('Full object'),
        1 => t('Preview'),
      ),
      '#title' => t('Render mode'),
      '#default_value' => $this->options['preview'],
    );
  }
}
