OOX module
==========

About
-----

OOX stands for Object Oriented eXperience. This module adds a basic therefore
useful set of OOP library for Drupal modules.

Error handling
--------------

All the error handling is exception based, you will have to handle every
exceptions the code can throw even in the lowest Drupal hook implementation
level in order to correctly handle errors. If you don't, you'll expose yourself
to a wide amount of WSOD. This is OOP code, think OOP.

Exceptions are always defined in the same file that classes that throws it, and
the PHP doc is as complete as we have time to write it, so any exception thrown
by any method will be described in it.

Registry
--------

@TODO

Library handling
----------------

A passthrough method able to lookup for external dependencies stored into the
library subfolder of your Drupal site exists. This method lookup for a series
of path until it finds the asked library and stored the real path (relative to
Drupal root to allow JS and CSS inclusion) into a static/persistent cache to
avoid further file lookup on library load.

See the oox_library_path() function.
