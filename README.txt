
XoXo module
===========

About
-----

XoXo is a non subtile reference to the Gossip Girl tremendous TV series. The
real module name should be "OOXO" which stands for Object Oriented eXperience
Objects.

This is an API meant to be used over the OOX module that provides arbitrary
OOP objects handling and persistance API.

Factory
-------

This module adds a common code base which implement the factory pattern in order
for module developers to easily share this common code amongs a consistent API
set.

This code is full OOP code, no big and ugly descriptions array in hooks, except
one which is your custom factory description, beyond that, all other stuff is
and will remain OOP code only.

Default object factories provides basic loading, searching, internal static
caching saving and deleting functions. It's easy to override and use.

It also defines some concepts that superseed Drupal's one, like the 'renderable'
interface and misc other stuff.

Exportables and features
------------------------

As you could see in 'ctools' module suite, this module also superseed the
'exportable objects' concept, based on unique machine name, providing an full
automatic features module XoXo without any further work to do.

Exportables objects comes along with the 'default' concept, which is the fact
you can hard-code any object and let your module provide it as 'default' usable
object the user can override later.

Administration
--------------

It provides some simple web administration pages for your objects, that you also
can override, in order for the user to easily manage its objects.

Views and Yamm
--------------

This module provides an automatic views integration for each object type defined
by any custom module. It will provide the necessary amount of default fields and
handlers, basically everything that you need for basic stuff.
Each object type will define its associated base table, with a few amount of
default relationships, even if you don't override views integration you will, at
least, be able to link with the user table.

By providing a base table-based views integration, it will also allows us to
provide an automatic Yamm integration. This is still to do and will come soon.

Goodies
-------

This module administration pages are really better looking when the Vertical
Tabs module is enabled.
