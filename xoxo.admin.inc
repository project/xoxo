<?php

/**
 * @file
 * XoXo Admin UI pages.
 * 
 * Notice that in all form functions we will prefer to use factory cached
 * description rather than the object implemented interfaces. We do this
 * because in those descriptions, the developper could had disabled some
 * features support to tune these forms, but keeping the interfaces
 * implementation in order to pragmatically use them in his code.
 * 
 * This will be the one and only case we use those description, in the OOP
 * API library, we will rely on objects interfaces implementations instead,
 * to ensure the best behavior.
 */

// Default limit for listing.
define('XOXO_OBJECT_ADMIN_LIMIT', 16);

/**
 * Helper to build some url.
 * 
 * @param XoxoObject $object
 * 
 * @return int|string
 *   Object oid if not exportable, else object unique name.
 */
function _xoxo_factory_admin_get_identifier($object) {
  if ($object instanceof XoxoExportableObject) {
    try {
      return $object->getName();
    }
    catch (OoxException $e) { }
  }
  else if ($object instanceof XoxoObject) {
    try {
      return $object->getIdentifier();
    }
    catch (OoxException $e) { }
  }
  return FALSE;
}

/**
 * Ajax callback for overview form that toggles an object status.
 */
function xoxo_factory_ajax_toggle_status($type, $object) {
  try {
    switch ($object->getStatus()) {
      case XoxoObject::STATUS_DISABLED:
        $status = XoxoObject::STATUS_ENABLED;
        break;
      case XoxoObject::STATUS_ENABLED:
      default:
        $status = XoxoObject::STATUS_DISABLED;
        break;
    }
    $object->setStatus($status);
    xoxo_factory_get($type)->saveStatus($object);
    print drupal_to_js(array('success' => TRUE, 'status' => $status));
  }
  catch (OoxException $e) {
    print drupal_to_js(array('success' => FALSE));
  }
  exit();
}

/**
 * Global overview form, can display either a named objects listing, or an
 * integer identified objects listing.
 */
function xoxo_factory_global_overview_form($form_state, $type) {
  $form = array();

  // Set some variable we could want to get back in validation and submission form.
  $form['#object_type'] = $type;
  $form['#theme'] = 'xoxo_factory_global_overview_form';

  // Load associated object factory.
  $factory = xoxo_factory_get($type);

  // Allow custom factories to set an help message in this form.
  $form['help'] = array(
    '#type' => 'markup',
    '#value' => $factory->getOverviewHelp(),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  // Create an array of loaded objects.
  $form['objects'] = array();
  foreach ($factory->loadAll(XOXO_OBJECT_ADMIN_LIMIT, FALSE) as $object) {
    // Get the identifier, it can be different weither the object is exportble
    // or not. This identifier will be used for all link path. The menu routeur
    // load function is able to loaded by oid or name indifferently so this is
    // only cosmetics.
    $identifier = _xoxo_factory_admin_get_identifier($object);
    $form['objects'][$identifier] = array();

    // Set enabled checkbox, this will be override by theme function in order to
    // nicely use ajax with it.
    $enabled = $object->isEnabled();
    $form['objects'][$identifier]['enabled'] = array(
      '#type' => 'checkbox',
      '#attributes' => array('class' => 'xoxo-enabled'), 
      '#default_value' => $enabled,
    );
    $form['objects'][$identifier]['identifier'] = array('#type' => 'markup', '#value' => $identifier);

    // Compute description field, with preview link.
    $description = $object->getDescription();
    if (!$description) {
      $description = t("Overview");
    }
    $description = l($description, 'admin/objects/' . $type . '/' . $identifier, array('attributes' => array('title' => t("Preview"))));
    $form['objects'][$identifier]['description'] = array('#type' => 'markup', '#value' => $description);

    // Set owner link.
    $form['objects'][$identifier]['owner'] = array('#type' => 'markup', '#value' => theme('username', $object->getOwner()));

    // Operations.
    $links = array();
    if ($object->isDefault()) {
      $links[] = array(
        'href' => 'admin/objects/' . $type . '/' . $identifier . '/edit',
        'title' => t('Override'),
      );
    }
    else {
      $links[] = array(
        'href' => 'admin/objects/' . $type . '/' . $identifier . '/edit',
        'title' => t('Edit'),
      );
      $links[] = array(
        'href' => 'admin/objects/' . $type . '/' . $identifier . '/delete',
        'title' => t('Delete'),
      );
    }
    if (oox_object_type_is_exportable($type)) {
      $links[] = array(
        'href' => 'admin/objects/' . $type . '/' . $identifier . '/export',
        'title' => t('Export'),
      );
    }
    $form['objects'][$identifier]['operations'] = array(
      '#type' => 'markup',
      '#value' => theme('links', $links),
    );
  }
  $form['pager'] = array('#type' => 'markup', '#value' => theme('pager', array()));
  return $form;
}

/**
 * Import form, type dependant.
 */
function xoxo_factory_global_import_form($form_state, $type) {
  $form = array();
  $form['#object_type'] = $type;
  $form['import'] = array(
    '#type' => 'textarea',
    '#title' => t("Exported PHP code"),
    '#rows' => 10,
    '#description' => t("Cut and paste exported code here to import in this site."),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Import"),
  );
  return $form;
}

/**
 * Import form validate handler.
 */
function xoxo_factory_global_import_form_validate($form, &$form_state) {
  try {
    xoxo_factory_get($form['#object_type'])->importCode($form_state['values']['import'], TRUE);
  }
  catch (Exception $e) {
    form_set_error('import', $e->getMessage());
  }
}

/**
 * Import form submit handler.
 */
function xoxo_factory_global_import_form_submit($form, &$form_state) {
  $objects = xoxo_factory_get($form['#object_type'])->importCode($form_state['values']['import']);
  foreach ($objects as $object) {
    drupal_set_message(t("<strong>@object</strong> has been saved.", array('@object' => $object->getName())));
  }
  if (count($objects) == 1) {
    drupal_goto('admin/objects/' . $type . '/' . array_shift($objects)->getName());
  }
  else {
    drupal_goto('admin/objects/' . $type);
  }
}

/**
 * Add, Edit, Override form.
 */
function xoxo_factory_object_edit_form($form_state, $type, $object = NULL) {
  $form = array('#tree' => TRUE);

  $factory = xoxo_factory_get($type);

  // Prepare some variables.
  $form['#object_type'] = $type;
  $form['#object_create'] = TRUE;
  $form['#object'] = NULL;

  if ($object) {
    try {
      $identifier = _xoxo_factory_admin_get_identifier($object);
      if (!$object->isDefault()) {
        $label = t("Update");
      }
      else {
        $label = t("Override");
      }
      $form['#object_create'] = FALSE;
      $form['#object'] = $object;
    }
    catch (Exception $e) {
      $form['#object'] = $factory->createInstance();
      $label = t('Add');
    }
  }
  else {
    $form['#object'] = $factory->createInstance();
    $label = t('Add');
  }

  // Real form.
  if ($form['#object'] instanceof XoxoExportableObject) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t("Name"),
      '#description' => t("This is the internal name. Only only alpha-numerical characters, and '_' or '-' are allowed."),
      '#disabled' => !$form['#object_create'],
    );
    if (!$form['#object_create']) {
      $form['name']['#value'] = $object->getName();
    }
    else {
      $form['name']['#default_value'] = '';
    }
  }

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t("Description"),
    '#description' => t("Short description about this object."),
    '#default_value' => ($object ? $object->getDescription() : ''),
  );

  // Specific object subform.
  if (oox_object_type_is_formable($type)) {
    $form['object_form'] = $form['#object']->form();
  }

  $form['buttons'] = array(
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div><div class="clear-block"></div>',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => $label,
  );

  if (isset($_GET['destination']) || isset($form_state['redirect'])) {
    // Choose the right path.
    $path = isset($_GET['destination']) ? $_GET['destination'] : $form_state['redirect'];
    $form['buttons']['cancel'] = array(
      '#type' => 'markup',
      '#value' => l(t("Cancel"), $path),
    );
  }
  return $form;
}

/**
 * Edit form validate handler.
 */
function xoxo_factory_object_edit_form_validate($form, &$form_state) {
  if (isset($form_state['values']['name'])) {
    if (!preg_match('/^[a-zA-Z0-9_-]+$/', trim($form_state['values']['name']))) {
      form_set_error('name', t("Title contains illegal characters."));
    }
  }
  // Specific object validation.
  if (isset($form_state['values']['object_form'])) {
    $form['object_form'] = $form['#object']->formValidate($form_state['values']['object_form']);
  }
}

/**
 * Edit form submit handler.
 */
function xoxo_factory_object_edit_form_submit($form, &$form_state) {
  try {
    $description = trim($form_state['values']['description']);
    // Load instance and fill it with form values.
    $object = $form['#object'];
    if (isset($form_state['values']['name'])) {
      $object->setName(trim($form_state['values']['name']));
    }
    $object->setDescription($description);
    // Specific object submission.
    if (isset($form_state['values']['object_form'])) {
      $form['object_form'] = $form['#object']->formSubmit($form_state['values']['object_form']);
    }
    // In case we are saving the object, set the owner might be a good idea.
    // FIXME: May be this should be exposed in the form.
    if ($form['#object_create']) {
      global $user;
      $object->setOwner($user);
    }
    // Save.
    $factory = xoxo_factory_get($form['#object_type']);
    $ret = $factory->save($object);
    $identifier = _xoxo_factory_admin_get_identifier($object);
    // In order to be the more accurate possible, we display the status message
    // following the save() method return, not using the form parameters, that
    // could be asynchronized will real object state.
    switch ($ret) {
      case XoxoFactory::SAVED_NEW:
        drupal_set_message(t("Object @object has been added.", array('@object' => $identifier)));
        break;
      case XoxoFactory::SAVED_UPDATED:
        drupal_set_message(t("Object @object has been updated.", array('@object' => $identifier)));
        break;
      case XoxoFactory::SAVED_ERROR:
        drupal_set_message(t("Object @object could not be saved.", array('@object' => $identifier)), 'error');
        break;
    }
    // Allow caller to get back the new object identifier as GET parameter.
    $form_state['redirect'] = 'admin/objects/' . $form['#object_type'] .'/' . $identifier;
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Export object form.
 */
function xoxo_factory_object_export_form($form_state, $type, $object) {
  $form = array();
  $form['#object_type'] = $type;
  $form['export'] = array(
    '#type' => 'textarea',
    '#title' => t("Exported PHP code"),
    '#rows' => 10,
    '#description' => t("Cut and paste this code into any other Drupal installation import form, or in any custom module code."),
  );
  $form['export']['#default_value'] = implode("\n", xoxo_factory_get($type)->exportSingle($object));
  return $form;
}

/**
 * Delete object confirmation form.
 */
function xoxo_factory_object_delete_form($form_state, $type, $object) {
  $form = array('oid' => array('#type' => 'value', '#value' => $object->getIdentifier()));
  $form['#object_type'] = $type;
  $factory = xoxo_factory_get($type);
  $description = NULL;
  // Try to instrospect for a default.
  if ($factory instanceof XoxoExportableFactory) {
    $name = $object->getName();
    if ($factory->existsAsDefault($name)) {
      $description = t("This object will be reverted to default, any manual modifications will be loosed.");
    }
    $question = t("Are you sure you want to delete the <em>@object</em> object?", array(
      '@object' => $name,
    ));
  }
  else {
    $question = t("Are you sure you want to delete this object ?");
  }
  if (!$description) {
    $description = t("This object will be definitively deleted.");
  }
  return confirm_form($form, $question, 'admin/objects/' . $type . '/overview', $description);
}

/**
 * Delete object confirmation form submit handler.
 */
function xoxo_factory_object_delete_form_submit($form, &$form_state) {
  xoxo_factory_get($form['#object_type'])->deleteByIdentifier($form_state['values']['oid']);
  $form_state['redirect'] = 'admin/objects/' . $form['#object_type'] . '/overview';
}

/**
 * Object preview page.
 */
function xoxo_factory_object_preview_page($type, $object) {
  return theme('xoxo_object', $object, TRUE);
}

/**
 * Global libraries configuration form.
 */
function xoxo_library_manage($form_state) {
  $form = array('#theme' => 'xoxo_library_manage', '#tree' => TRUE);
  $form['buttons'] = array();
  $form['libraries'] = array();
  // Iterate over known libraries in order to build the recapulative table.
  foreach (_oox_library_get_all() as $library => $pathes) {
    $form['libraries'][$library] = array();
    foreach ($pathes as $file => $path) {
      $form['libraries'][$library][$file] = array('#type' => 'markup', '#value' => $path);
    }
  }
  // Add some operations buttons.
//  $form['buttons']['submit'] = array(
//    '#type' => 'submit',
//    '#value' => t("Save configuration"),
//  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Clear library cache"),
    '#submit' => array('xoxo_registry_manage_submit_clear_cache'),
  );
  return $form;
}

/**
 * Submit handler for library form clear cache button.
 */
function xoxo_registry_manage_submit_clear_cache($form, &$form_state) {
  cache_clear_all('oox_library_path', OOX_CACHE_TABLE);
  drupal_set_message(t("Library cache has been cleared, cache will be completed each time a contribution module asks for an external library."));
}

/**
 * Global registries configuration form.
 */
function xoxo_registry_manage($form_state) {
  $form = array('#theme' => 'xoxo_registry_manage', '#tree' => TRUE);

  // Prepare form.
  $form['names'] = array();
  $form['weights'] = array();

  // Populate weights table.
  foreach (OoxRegistry::_getAllDescriptions() as $type => $desc) {
    // Set first weight to 0.
    $currentWeight = 0;

    foreach (OoxRegistry::getInstance($type)->getItemCache() as $itemType => $item) {
      if (!isset($form['names'][$type])) {
        $form['names'][$type] = array();
        $form['weights'][$type] = array();
        // Prepare table title.
        $form['names'][$type]['#title'] = t("@type items weights", array('@type' => isset($desc['name']) ? t($desc['name']) : $type));
      }

      try {
        // Set form values.
        $form['names'][$type][$itemType] = array('#type' => 'markup', '#value' => isset($item['name']) ? t($item['name']) : $itemType);
        $form['weights'][$type][$itemType] = array('#type' => 'hidden', '#default_value' => $currentWeight++);
      }
      catch (OoxException $e) {
        // FIXME: notify errors.
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save configuration"),
  );

  return $form;
}

/**
 * Submit handler for global registries configuration.
 */
function xoxo_registry_manage_submit($form, &$form_state) {
  foreach ($form_state['values']['weights'] as $type => &$data) {
    try {
      $registry = OoxRegistry::getInstance($type);
      variable_set($registry->_getHookName() . '_weight', $data);
      // Explicitely clear and rebuild item cache.
      $registry->_rebuildItemCache();
    }
    catch (OoxException $e) {
      // FIXME: At least watchdog here.
    }
  }
  drupal_set_message(t("Registries item weights have been changed."));
}

/**
 * Global configuration form.
 */
function xoxo_factory_manage($form_state) {
  $form = array();

  $form[OOX_VAR_LOCK_BACKEND] = array(
    '#type' => 'select',
    '#title' => t("Lock backend"),
    '#description' => t("Depending on your environment, some backends could be faster than others. If you are using memcache or APC as Drupal core cache backend, you might want to switch to the cache backend. Default is database backend, and will work for all users."),
    '#options' => array(
      0 => t("Database backend"),
      // 'OoxLockCache' => t("Cache backend"),
    ),
    '#default_value' => variable_get(OOX_VAR_LOCK_BACKEND, 0),
  );

  $form['buttons'] = array();
  $form['buttons']['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t("Rebuild object cache"),
    '#attributes' => array(
      'title' => t("This will rebuild the object cache without flushing the full Drupal cache. Notice that it will also clear the menu cache in order for items to display correctly in administration pages. It can help in some edge cases where your cache did not rebuild correctly."),
    ),
    '#submit' => array('xoxo_factory_manage_submit_rebuild'),
  );

  return system_settings_form($form);
}

function xoxo_factory_manage_submit_rebuild($form, &$form_state) {
  cache_clear_all('xoxo_factories', OOX_CACHE_TABLE);
  menu_rebuild();
  drupal_set_message(t("Object cache cleared."));
}

/**
 * Theme function for global overview form.
 */
function theme_xoxo_factory_global_overview_form($form) {
  // Provide UI alteration that will enable ajax nice stuff.
  drupal_add_js(drupal_get_path('module', 'xoxo') . '/xoxo.admin.js');
  drupal_add_js(array('Xoxo' => array(
    'ajaxPrefixUrl' => url('admin/objects/' . $form['#object_type'] . '/ajax'),
  )), 'setting');
  drupal_add_css(drupal_get_path('module', 'xoxo') . '/xoxo.admin.css');
  // Prepare table rendering.
  $headers = array(t("Enabled"), t("Identifier"), t("Description"), t('Owner'), t("Operations"));
  $rows = array();
  foreach (element_children($form['objects']) as $key) {
    // Extract some useful vars.
    $enabled = (bool) $form['objects'][$key]['enabled']['#default_value'];
    $name = $form['objects'][$key]['identifier']['#value'];
    // Prepare current row.
    $row = array(
      'data' => array(),
      // Helpers for css/js/ajax handling.
      'class' => 'xoxo-object-row ' . ($enabled ? 'xoxo-object-row-enabled' : 'xoxo-object-row-disabled'),
      'rel' => $name,
    );
    // Set some already computed cells.
    $row['data'][] = drupal_render($form['objects'][$key]['enabled']);
    $row['data'][] = drupal_render($form['objects'][$key]['identifier']);
    $row['data'][] = drupal_render($form['objects'][$key]['description']);
    $row['data'][] = drupal_render($form['objects'][$key]['owner']);
    $row['data'][] = drupal_render($form['objects'][$key]['operations']);
    $rows[] = $row;
  }
  return theme('table', $headers, $rows) . drupal_render($form);
}

/**
 * Theme library form.
 */
function theme_xoxo_library_manage($form) {
  // Known libraries, just a recapitulative table of known libraries.
  $output = '<h2>' . t("Known libraries") . '</h2>';
  $headers = array(t("Library"), t("File"), t("Full path"));
  $rows = array();
  // Iterate over libraries.
  foreach (element_children($form['libraries']) as $library_key) {
    $first = TRUE;
    $pathes = element_children($form['libraries'][$library_key]);
    foreach ($pathes as $path_key) {
      if ($first == TRUE) {
        // Build the first row, using a rowspan for the first cell.
        $rows[] = array(array(
          'data' => check_plain($library_key),
          'rowspan' => count($pathes),
        ), array(
          'data' => '<strong>' . check_plain($path_key) . '</strong>',
        ), array(
          'data' => drupal_render($form['libraries'][$library_key][$path_key]),
        ));
        $first = FALSE;
      }
      else {
        // Build the array of other rows.
        $rows[] = array(array(
          'data' => '<strong>' . check_plain($path_key) . '</strong>',
        ), array(
          'data' => drupal_render($form['libraries'][$library_key][$path_key]),
        ));
      }
    }
  }
  $output .= theme('table', $headers, $rows);
  // Render the normal administrative form.
  $output .= '<h2>' . t("Operations") . '</h2>';
  return $output . drupal_render($form);
}

/**
 * Theme for registries weights form.
 */
function theme_xoxo_registry_manage($form) {
  $elements = array();
  // Iterate on registries.
  foreach (element_children($form['names']) as $type) {
    // Prepare table.
    $title = $form['names'][$type]['#title'];
    $rows = array();
    // Iterate on items descriptions.
    foreach (element_children($form['names'][$type]) as $itemType) {
      $form['weights'][$type][$itemType]['#attributes']['class'] = 'registry-item-order-weight';
      $rows[] = array(
        'data' => array(
          drupal_render($form['names'][$type][$itemType]) . drupal_render($form['weights'][$type][$itemType])
        ),
        'class' => 'draggable',
      );
    }
    // Compute arbitrary CSS identifier for the current table.
    $key = 'registry-' . $type . '-order';
    // Put the table into a fieldset.
    $element = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => $title,
      'content' => array(
        '#type' => 'markup',
        '#value' => theme('table', NULL, $rows, array('id' => $key)),
      ),
    );
    $elements[] = $element;
    drupal_add_tabledrag($key, 'order', 'sibling', 'registry-item-order-weight', NULL, NULL, FALSE);
  }
  // Vertical tab usage
  if (module_exists('vertical_tabs')) {
    vertical_tabs_add_vertical_tabs($elements);
  }
  // Final rendering.
  return drupal_render($elements) . drupal_render($form);
}
