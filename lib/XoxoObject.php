<?php

class XoxoObject extends Options
{
  const STATUS_DISABLED = 0;
  const STATUS_ENABLED = 1;

  /**
   * Return this particular object factory.
   * 
   * @return XoxoFactory
   *   Specific factory implementation.
   */
  public function getFactory() {
    // FIXME: Don't like this implementation. Could be better.
    return OoxRegistry::getInstance('factory')->getFactoryForObject($this);
  }

  /**
   * Module that owns this object, will be used only for default objects.
   * This is an internal property, and should only be used only by the
   * XoxoFactory instance.
   * 
   * @var string
   */
  protected $_module;

  /**
   * Get owner module.
   */
  public final function getModule() {
    return $this->_module;
  }

  /**
   * Set owner module.
   */
  public final function setModule($module) {
    $this->_module = $module;
  }

  /**
   * Does this object is default provided by a module.
   * 
   * @var boolean
   */
  private $__default = FALSE;

  /**
   * Change default state. This is for internal use, never set it yourself.
   * 
   * @param boolean $toggle
   *   TRUE if object is default provided by a module, FALSE else.
   */
  public function setDefault($toggle) {
    $this->__default = (bool) $toggle;
  }

  /**
   * Tells if object is default provided by a module.
   * 
   * @return boolean
   */
  public function isDefault() {
    return $this->__default;
  }

  /**
   * Unique int identifier.
   * 
   * @var int
   */
  protected $_oid;

  /**
   * Set unique idendifier.
   * 
   * This is an internal method for factories, you must never set this manually,
   * like node nid, this is an automatic serial value from database.
   * 
   * @param int $oid
   */
  public function setIdentifier($oid) {
    $this->_oid = $oid;
  }

  /**
   * Get unique idendifier.
   * 
   * @return int
   * 
   * @throws XoxoException
   *   If the object does not exists in database.
   */
  public function getIdentifier() {
    if (!isset($this->_oid) || empty($this->_oid)) {
      throw new OoxException("Identifier not set.");
    }
    return $this->_oid;
  }

  /**
   * @var string
   */
  protected $__description;

  /**
   * Set human readable description.
   * 
   * @param string $description
   *   Human readable description.
   */
  public function setDescription($description) {
    $this->__description = $description;
  }

  /**
   * Get human readable description.
   * 
   * @return string
   */
  public function getDescription() {
    return $this->__description;
  }

  /**
   * Current status. 0 is disable, any other is enabled.
   * 
   * @var int
   */
  private $__status = self::STATUS_ENABLED;

  /**
   * Get object status.
   */
  public function getStatus() {
    return $this->__status;
  }

  /**
   * Set object status.
   * 
   * @param int $status
   *   New status.
   */
  public function setStatus($status) {
    $this->__status = (int) $status;
  }

  /**
   * Tells if object is enabled.
   * 
   * Alias to getStatus(), returns a boolean instead of an integer value.
   * 
   * @return boolean
   */
  public function isEnabled() {
    return $this->__status != self::STATUS_DISABLED;
  }

  /**
   * Object owner.
   * 
   * @var int
   *   Owner uid.
   */
  private $__uid = 0;

  /**
   * Get owner uid without trying to load it.
   *  
   * @return int
   *   Owner uid, can be 0 (anonymous).
   */
  public function getOwnerUid() {
    return $this->__uid;
  }

  /**
   * Get object owner.
   * 
   * @return object
   *   Drupal account object or FALSE if anonymous account.
   */
  public function getOwner() {
    if (is_numeric($this->__uid) && $account = user_load($this->__uid)) {
      return $account;
    }
    return FALSE;
  }

  /**
   * Set object owner.
   * 
   * @param int|object $account
   *   Owner uid or account object.
   */
  public function setOwner($account) {
    $this->__uid = is_object($account) ? $account->uid : $account;
  }

  /**
   * Default constructor.
   * 
   * @param string $description = NULL
   *   (optional) Human readable description.
   */
  public function __construct($description = NULL) {
    if ($description) {
      $this->setDescription($description);
    }
    $this->setDefault(FALSE);
  }

  /**
   * Cleanup instance before saving. You can restore any information at load
   * time using the restore() method.
   */
  public function clean() {
    // FIXME: This might not be the prettiest thing we could do, but we won't
    // store useless data.
    $this->_options = array();
  }

  /**
   * Object is being loaded from the database, you can restore things you have
   * cleaned up at clean() time.
   */
  public function restore() { }

  /**
   * Render the preview page.
   */
  public function renderPreview() { return $this->getName(); }

  /**
   * Duplicate this instance without identifiers and unique keys.
   * 
   * @return XoxoObject
   */
  public function duplicate() {
    $object = clone($this);
    $object->setIdentifier(NULL);
    $object->setStatus(self::STATUS_ENABLED);
    $object->setOwner(0);
    $object->setDefault(FALSE);
    return $object;
  }
}
