<?php

/**
 * Defines basis for any kind of complex objets. 
 */
class XoxoExportableFactory extends XoxoFactory
{
  /**
   * Maintain a loaded object cache.
   * 
   * @var array
   */
  protected $_loadedNames = array();

  /**
   * Override the default constructor. This in order to save defaults as objects
   * in database for paging queries to work as any others.
   */
  public function __construct(&$item) {
    parent::__construct($item);
    // Check for defaults cache.
    if (!variable_get('xoxo_defaults_loaded', FALSE)) {
      $this->saveDefaults();
    }
  }

  /**
   * Get all defaults.
   */
  public function saveDefaults() {
    $hook = $this->_type . '_defaults';
    foreach (module_implements($hook) as $module) {
      foreach (module_invoke($module, $hook) as $object) {
        try {
          // For each default, check if an override exists, if not, save it into
          // database.
          if (!$this->existsByName($object->getName())) {
            // Explicitely set the default boolean.
            $object->setDefault(TRUE);
            // The save() method will update internal static cache, so there is
            // nothing else to care about here.
            $this->save($object);
          }
        }
        catch (OoxException $e) {
          // Any exception here tells that the default definition is incomplete
          // so we just ignore it, but warn the developer he does an error.
          watchdog('xoxo', "A default object defined by module " . $module . " has a wrong definition (check if name is set), ignoring it.", NULL, WATCHDOG_ERROR);
        }
      }
    }
  }

  /**
   * (non-PHPdoc)
   * @see XoxoFactory::_populate()
   */
  protected function _populate($row) {
    $object = parent::_populate($row);
    if ($object instanceof XoxoExportableObject) {
      $object->setName($row->name);
    }
    return $object;
  }

  /**
   * (non-PHPdoc)
   * @see XoxoFactory::_objectLoaded()
   */
  protected function _objectLoaded(XoxoObject $object) {
    parent::_objectLoaded($object);
    $this->_loadedNames[$object->getName()] = $object;
  }

  /**
   * (non-PHPdoc)
   * @see XoxoFactory::_loadAllExcludeFromCache()
   */
  protected function _loadAllExcludeFromCache() {
    $ret = parent::_loadAllExcludeFromCache();
    $ret['name'] = array_keys($this->_loadedNames);
    return $ret;
  }

  /**
   * Load layout by machine name.
   * 
   * @param string $name
   *   Machine name.
   * 
   * @return XoxoExportableObject
   *   XoxoExportableObject instance.
   * 
   * @throws XoxoFactoryException
   *   If layout does not exists.
   */
  public function loadByName($name) {
    $object = NULL;
    if (!isset($this->_loadedNames[$name])) {
      // Load from database aptempt.
      if ($data = db_fetch_object(db_query("SELECT * FROM {%s} WHERE name = '%s'", $this->getTableName(), $name))) {
        $this->_objectLoaded($this->_populate($data));
      }
      else {
        throw new XoxoFactoryException("Object named '" . $name . "' does not exists, if you are expecting a default, try to wipe out your site caches");
      }
    }
    $ret = NULL;
    switch ($this->_loadMethod) {
      case self::LOAD_METHOD_CLONE:
        $ret = clone($this->_loadedNames[$name]);
        break;
      case self::LOAD_METHOD_INSTANCE:
      default:
        $ret = &$this->_loadedNames[$name];
    }
    return $ret;
  }

  /**
   * Tells if current layout name exists as a default layout.
   * 
   * @return boolean
   */
  public function existsAsDefault($name) {
    // FIXME: This is kind of ugly, and could be optimized.
    $hook = $this->_type . '_defaults';
    foreach (module_implements($hook) as $module) {
      foreach (module_invoke($module, $hook) as $object) {
        if ($object->getName() == $name) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * (non-PHPdoc)
   * @see XoxoFactory::_preSave()
   * 
   * Last chance object save alteration, set our own real object name.
   */
  protected function _preSave(XoxoObject $object, &$data) {
    parent::_preSave($object, $data);
    $data['name'] = $object->getName();
  }

  /**
   * Delete given layout.
   * 
   * If layout does not exists, this method will remain silent.
   * 
   * @param string $name
   *   XoxoExportableObject name.
   */
  public function deleteByName($name) {
    return $this->deleteByField('name', $name);
  }

  /**
   * Check if given machine name already exists in database.
   * 
   * @param string $name
   *   Machine name.
   * 
   * @return boolean
   */
  public function existsByName($name) {
    return (bool) db_result(db_query("SELECT 1 FROM {%s} WHERE name = '%s'", $this->getTableName(), $name));
  }

  /**
   * Import layout from raw PHP code.
   * In case of any error, no records at all will be saved.
   * 
   * Be careful, if any of these layouts exists, it will overwrite the
   * existing one.
   * 
   * Note for developers: when you override this method, be careful about the
   * $pretend parameter, and dont forget to call the XoxoFactory::save()
   * method in order to save new objects.
   * 
   * @param string $code
   *   Raw PHP code returning a XoxoExportableObject instance, or an array of
   *   XoxoExportableObject instances.
   * @param boolean $pretend = FALSE
   *   Set this to TRUE if you don't want code to be actually imported. This
   *   can be usefull for forms validation phase.
   * 
   * @throws XoxoFactoryException
   *   In case of any error. Error message will be a localized human readable
   *   error message and will be displayed to end use in the import form.
   * 
   * @return array
   *   The array of new imported layouts.
   */
  public function importCode($code, $pretend = FALSE) {
    throw new XoxoFactoryException("Not implemented");
  }

  /**
   * Export the given object to raw PHP code.
   * 
   * This is the method that specific factories should override. This simple
   * implementation will be enougth for objects using only options as storage.
   * 
   * @param XoxoExportableObject $object
   *   An XoxoExportableObject object specific instance.
   * 
   * @return array
   *   An array of PHP code lines.
   * 
   * @throws XoxoFactoryException
   *   In case any error happens.
   */
  public function exportSingle(XoxoExportableObject $object, $objectName = 'object') {
    $code = array();

    // Set common properties, exlude the oid..
    $code[] = "\$" . $objectName . " = new " . $this->_className . "();";
    $code[] = "\$" . $objectName . "->setStatus(" . $object->getStatus() .  ");";
    $code[] = "\$" . $objectName . "->setOwner(" . $object->getOwnerUid() .  ");";
    $code[] = "\$" . $objectName . "->setName('" . stripslashes($object->getName()) .  "');";
    if ($module = $object->getModule()) {
      $code[] = "\$" . $objectName . "->setModule('" . stripslashes($module) .  "');";
    }
    if ($description = $object->getDescription()) {
      $code[] = "\$" . $objectName . "->setDescription('" . stripslashes($description) .  "');";
    }

    foreach ($object->getOptions() as $name => $value) {
      if (is_string($value)) {
        $code[] = "\$" . $objectName . "->setOption('" . $name . "', '" . stripslashes($value) .  "');";
      }
      else if (is_numeric($value)) {
        $code[] = "\$" . $objectName . "->setOption('" . $name . "', " . $value .  ");";
      }
      else {
        $code[] = "\$" . $objectName . "->setOption('" . $name . "', " . serialize($value) .  ");";
      }
    }

    return $code;
  }

  /**
   * Export the given object(s) to raw PHP code.
   * 
   * @param array $object
   *   Array of XoxoExportableObject object instances.
   * @param boolean $returnStatement = TRUE
   *   (optional) Append or not a return statement at the end of the code.
   * @param int $indent = 0
   *   (optional) Number of space to indent the code.
   * 
   * @return array
   *   An array of PHP line codes.
   * 
   * @throws XoxoFactoryException
   *   In case any error happens.
   */
  public final function exportMutiple($objects, $returnStatement = TRUE, $indent = 0) {
    $code = array();
    $code[] = "\$items = array();";
    $code[] = "";

    foreach ($objects as $object) {
      $code = array_merge($code, $this->exportSingle($object, 'object'));
      $code[] = "\$items[] = \$object;";
      $code[] = "";
    }

    if ($returnStatement) {
      $code[] = "return \$items;";
    }

    return $code;
  }

  /**
   * Allow sub modules to expose new dependencies for the feature module
   * xoxo.
   * 
   * This is absolutely not mandatory, but if you intend to use the import
   * and export facilities, you are strongly adviced to implement this.
   * 
   * @see hook_features_export()
   * 
   * @param XoxoExportableObject $object
   *   The current object being exported
   * @param array &$pipe
   *   See features module documentation.
   * @param array &$export
   *   See features module documentation.
   * 
   * @throws XoxoFactoryException
   *   In case any error happens.
   */
  public function featuresDependencies($object, &$pipe, &$export) { }

  /**
   * (non-PHPdoc)
   * @see XoxoFactory::_cacheClear()
   */
  protected function _cacheClear() {
    parent::_cacheClear();
    $this->_loadedNames = array();
  }
}
