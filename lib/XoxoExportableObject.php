<?php

class XoxoExportableObject extends XoxoObject
{
  /**
   * @var string
   */
  protected $_name;

  /**
   * Set machine name.
   * 
   * @param string $name
   *   Machine name.
   */
  public function setName($name) {
    $this->_name = $name;
  }

  /**
   * Get machine name.
   * 
   * @return string
   * 
   * @throws OoxException
   *   If name not set.
   */
  public function getName() {
    if (!$this->_name) {
      throw new OoxException("Name not set.");
    }
    return $this->_name;
  }

  /**
   * Default constructor.
   * 
   * @param string $name = NULL
   *   (optional) Machine name. Use this parameter if you intend to save or
   *   export your layout later.
   * @param string $description = NULL
   *   (optional) Human readable description.
   */
  public function __construct($name = NULL, $description = NULL) {
    parent::__construct($description);
    if ($name) {
      $this->_name = $name;
    }
  }

  /**
   * (non-PHPdoc)
   * @see XoxoObject::duplicate()
   */
  public function duplicate() {
    $object = parent::duplicate();
    $object->setName(NULL);
    return $object;
  }
}
