<?php

/**
 * Registry implementation for factories. We are going to override the
 * getItem() method in order to maintain a pool of singletons instead
 * of creating new instances each time.
 */
class XoxoFactoryRegistry extends OoxRegistry
{
  /**
   * Introspect registries definition to find which factory own this item using
   * its class.
   *
   * @throws OoxRegistryException
   *   In case no associated registry found.
   */
  public function getFactoryForObject($object) {
    if (!is_object($object)) {
      throw new OoxRegistryException("Given object is not an object instance");
    }
    $objectClass = get_class($object);
    if ($class == 'stdClass') {
      throw new OoxRegistryException("Cannot introspect for a stdClass object");
    }
    foreach ($this->getItemCache() as $type => $item) {
      if ($item['class'] == $objectClass) {
        return $this->getItem($type);
      }
    }
    throw new OoxRegistryException("No factory found for object class " . $objectClass);
  }

  /**
   * (non-PHPdoc)
   * @see OoxRegistry::_checkItem()
   */
  protected function _checkItem($type, &$item, $module) {
    // Check implementation defines a target class.
    if (!isset($item['target'])) {
      watchdog('xoxo', "Module " . $module . " does not define any target class, unregistering object type " . $type, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
    $objectClass = $item['target'];
    // Check for the target class name.
    if (!oox_class_is($objectClass, 'XoxoObject')) {
      watchdog('xoxo', "Module " . $module . " define the target class " . $objectClass . " which does not exists or does not extends XoxoObject, unregistering object type " . $type, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
    // If no class name for the factory itself, use default implementation.
    if (!isset($item['class'])) {
      if (oox_class_is($objectClass, 'XoxoExportableObject')) {
        $item['class'] = 'XoxoExportableFactory';
      }
      else {
        $item['class'] = 'XoxoFactory';
      }
    }
    // This test must be done after we may have set a default class name.
    if (parent::_checkItem($type, $item, $module)) {
      // Check for administrable ability.
      if (!isset($item['administrable'])) {
        $item['administrable'] = FALSE;
      }
      // Check for import/export ability.
      if (!isset($item['exportable'])) {
        $item['exportable'] = oox_class_is($objectClass, 'XoxoExportableObject');
      }
      else if ($item['exportable'] && !oox_class_is($objectClass, 'XoxoExportableObject')) {
        $item['exportable'] = FALSE;
        watchdog('xoxo', "Module " . $module . " define the target class " . $objectClass . " as exportable, but does not extends XoxoExportableObject, forcing exportable parameter to FALSE", NULL, WATCHDOG_ALERT);
      }
      // Check for load method.
      if (!isset($item['load_method'])) {
        $item['load_method'] = XoxoFactory::LOAD_METHOD_INSTANCE;
      }
      // Check for renderable or formable.
      $item['renderable'] = oox_class_is($objectClass, 'IRenderable');
      $item['formable'] = oox_class_is($objectClass, 'IFormable');
      // We are all good, I presume.
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Singleton pattern override.
   * 
   * @var array
   */
  private $__instances = array();

  /**
   * (non-PHPdoc)
   * @see OoxRegistry::_getHookName()
   * 
   * Overrided in order to keep the old hook name.
   */
  public function _getHookName() {
    return 'xoxo_factory';
  }

  /**
   * getItem() method override, we are here going to implement a singleton
   * pattern for the current registry elements and ensure factories.
   * 
   * @see OoxRegistry::getItem()
   */
  public function getItem($type) {
    $this->_loadItemCache();
    // Check type exists.
    if (!isset($this->_cache[$type])) {
      throw new XoxoFactoryException("Factory type " . $type . " is unknown");
    }
    // Create new instance and return element if all ok.
    if (!isset($this->__instances[$type])) {
      $class = $this->_cache[$type]['class'];
      $this->__instances[$type] = new $class($this->_cache[$type]);
    }
    // Return the object.
    return $this->__instances[$type];
  }
}
