<?php

class XoxoFactoryException extends OoxException { }

/**
 * Default factory for XoxoObject instances.
 * 
 * The factory is able to create, read, update or insert any objects of the
 * factory target type, using transactionnal requests for create and update
 * operations.
 * Target type is a class name, given by the hook_xoxo_factory() module
 * custom implementation that defines it.
 * 
 * Factories are themselves registrered using the XoxoFactoryRegistry
 * specific OoxRegistry implementation. This is masked by some helpers
 * in the XoXo module such as xoxo_factory_get().
 * 
 * @see oox_factory_get()
 * @see XoxoFactoryRegistry
 * @see OoxRegistry
 */
class XoxoFactory
{
  // An error happend during save.
  const SAVED_ERROR = 0;
  // Object has been updated.
  const SAVED_UPDATED = 1;
  // Object has been saved.
  const SAVED_NEW = 2;

  // Clone object when loading it from cache.
  const LOAD_METHOD_CLONE = 1;
  // Get instance when loading it from cache.
  const LOAD_METHOD_INSTANCE = 2;

  // FIXME: Implement this.
  // Do not cache objects.
  const CACHE_METHOD_NONE = 0;
  // Cache object on a per-object basis.
  const CACHE_METHOD_OBJECT = 1;
  // Cache all known objects in one single cache entry.
  const CACHE_METHOD_ALL = 2;

  /**
   * Internal type name, usefull for the registry pattern.
   * 
   * @var string
   */
  private $__type = NULL;

  /**
   * Get internal type name.
   * 
   * @return string
   */
  public function getType() {
    return $this->__type;
  }

  /**
   * SQL table name.
   * 
   * @var string
   */
  private $__tableName;

  /**
   * Get table name.
   * 
   * Override this method if you don't define the table name at hook level.
   * 
   * @return string
   *   Table name.
   */
  public function getTableName() {
    if (!isset($this->__tableName)) {
      throw new XoxoFactoryException("No table name set.");
    }
    return $this->__tableName;
  }

  /**
   * Managed objects class name.
   * 
   * @var string
   */
  protected $_className;

  /**
   * Tells if target class is exportable.
   */
  public final function isExportable() {
    return is_subclass_of($this->_className, 'XoxoExportableObject');
  }

  /**
   * Cache method.
   * 
   * @var int
   */
  protected $_cacheMethod = self::CACHE_METHOD_NONE;

  /**
   * Load method.
   * 
   * @var int
   */
  protected $_loadMethod = self::LOAD_METHOD_INSTANCE;

  /**
   * Object type machine name.
   * 
   * @var string
   */
  protected $_type;

  /**
   * Construct the factory using the given table name.
   *
   * @param array $item
   *   Descriptive registry type array.
   */
  public function __construct(&$item) {
    $this->__tableName = $item['table'];
    $this->_className = $item['target'];
    $this->_type = $item['type'];
    if ($item['load_method']) {
      $this->_loadMethod = $item['load_method'];
    }
  }

  /**
   * Maintain a loaded object cache.
   * 
   * @var array
   */
  protected $_loaded = array();

  /**
   * Populate new instance using given database row.
   * This is an internal method, please avoid relying on it.
   * 
   * Any static cache fill at load time must be implemented in this method for
   * overriding caches, this is the only entry point for all object loading.
   * 
   * @param object $row
   *   Full row from database.
   * 
   * @return XoxoExportableObject
   *   New object from given row data.
   */
  protected function _populate($row) {
    $object = unserialize(db_decode_blob($row->data));
    if ($object instanceof XoxoObject) {
      // Override some parameters that could have been saved with database
      // specified data. This will allow modules to alter data in database
      // without having to load/save the full object and ensure the real
      // loaded instance will keep these settings.
      $object->setIdentifier($row->oid);
      $object->setOwner($row->uid);
      $object->setStatus($row->status);
      $object->setDefault($row->defaults);
      // Load object options, if any.
      OptionsFactory::getInstance()->load($row->oid, $object);
      // Fill the object cache.
      $this->_loaded[$row->oid] = $object;
      return $object;
    }
    return NULL;
  }

  /**
   * An object has been loaded, overriding classes should implement this in
   * order to update their internal static cache.
   * 
   * It could be done in _populate() method, but in some rare cases, objects
   * can be loaded without going through _populate().
   */
  protected function _objectLoaded(XoxoObject $object) {
    $object->restore();
  }

  /**
   * Load an object by identifier.
   * 
   * @param int $oid
   *   Identifier.
   * 
   * @return XoxoObject
   *   XoxoObject instance.
   * 
   * @throws XoxoFactoryException
   *   If layout does not exists.
   */
  public final function loadByIdentifier($oid) {
    $object = NULL;
    if (!isset($this->_loaded[$oid])) {
      // Load from database aptempt.
      if ($data = db_fetch_object(db_query("SELECT * FROM {%s} WHERE oid = %d", $this->getTableName(), $oid))) {
        $this->_objectLoaded($this->_populate($data));
      }
      else {
        throw new XoxoFactoryException("Object " . $oid . " does not exists.");
      }
    }
    $ret = NULL;
    switch ($this->_loadMethod) {
      case self::LOAD_METHOD_CLONE:
        $ret = clone($this->_loaded[$oid]);
        break;
      case self::LOAD_METHOD_INSTANCE:
      default:
        $ret = $this->_loaded[$oid];
    }
    return $ret;
  }

  /**
   * Load all objects owner by this particular user.
   * 
   * @see XoxoFactory::loadAll()
   *   For addition parameters.
   * 
   * @param int|object $account
   *   Account identifier or object.
   * @params ...
   *   XoxoFactory::loadAll() parameters.
   */
  public final function loadAllByOwner($account, $pagerLimit = 0, $excludeDisabled = TRUE, $pagerElement = 0) {
    $uid = is_object($account) ? $account->uid : $account;
    return $this->_loadAll($pagerLimit, $excludeDisabled, $pagerElement, array('uid' => $uid));
  }

  /**
   * Load all objects.
   * 
   * Warning: do not use this method if you can do otherwise. This could be
   * resource intensive if you do not use paging.
   * 
   * @param int $pagerLimit = 0
   *   (optional) Set here the limit for paged results. This will make the
   *   factory use the Drupal pager_query() in order to get back result, this
   *   particular case implies that the page parameter is given as GET param,
   *   so you won't have control over it.
   *   Notice: by using the pagerLimit, you deactivate the object exclusion
   *   mecanism from query to ensure you have a full range of objects.
   * @param boolean $excludeDisabled = TRUE
   *   (optional) If set to FALSE, disabled object will be load too. This is
   *   useful for administrator forms or administration pages.
   * @param int $pagerElement = 0
   *   (optional) For administration pages control, you can set here the pager
   *   element value.
   * 
   * @return array
   *   Array of XoxoObject. You can't rely on keys since the overriding
   *   classes may arbitrary add elements.   
   */
  public function loadAll($pagerLimit = 0, $excludeDisabled = TRUE, $pagerElement = 0) {
    return $this->_loadAll($pagerLimit, $excludeDisabled, $pagerElement);
  }

  /**
   * Method that should be overiden, this allows to exclude cached content
   * from loadAll() requests to avoid objects double loading.
   * 
   * This is run in loadAll() internal method, before additional filters
   * building.
   * 
   * This function will only be called if query is not paged. Dropping results
   * from the paged query would arbitrary change the result number for the
   * current page.
   * 
   * See this sample implementation to see how to implement it. Don't forget
   * to call parent::_loadAllExcludeFromCache() and merge your results when
   * you override it.
   * 
   * @return array
   *   Key/value pairs, keys are field names, values are an array of values
   *   to exclude for this field.
   */
  protected function _loadAllExcludeFromCache() {
    return array('oid' => array_keys($this->_loaded));
  }

  /**
   * Get Drupal schema API type for given field.
   * 
   * @return string
   *   Drupal schema API type or NULL if field does not exists.
   */
  protected final function _getFieldType($field) {
    $schema = drupal_get_schema($this->getTableName());
    if (isset($schema['fields'][$field])) {
      return $schema['fields'][$field]['type'];
    }
    return NULL;
  }

  /**
   * Get placeholder string for given field.
   * 
   * @return string
   *   Placeholder string or NULL if field does not exists.
   */
  protected final function _getFieldPlaceholder($field) {
    $schema = drupal_get_schema($this->getTableName());
    if (isset($schema['fields'][$field])) {
      return db_type_placeholder($schema['fields'][$field]['type']);
    }
    return NULL;
  }  

  /**
   * This will create the query conditions that matches the given access
   * permission you can then use as the $condition parameter for load
   * methods.
   * 
   * @param string $op
   *   'delete', 'update' or 'view'. Object creation here as no sens because
   *   only existing objects are going to be loaded from database.
   * @param int|object $account = NULL
   *   (optional) Account identifier or object. If NULL it will uses the
   *   current user for access checks.
   * 
   * @return array
   *   Key/value pairs of parameters. Keys are field name, and values are
   *   values to match in the query.
   *   Returns FALSE in case an invalid permission is given, or if the user
   *   has no right at all.
   */
  protected final function _getUserAccessConditions($op, $account = NULL) {
    // Check if an account was given. We need the full object structure
    // for the user_access method.
    if (!$account) {
      global $user;
      $account = $user;
    }
    else {
      $account = is_object($account) ? $account : user_load($account);
    }
    // Do the proper object exclusions following the permission.
    $perm_any = $op . ' any ' . $this->_type;
    $perm_own = $op . ' own ' . $this->_type;
    // Check user access.
    if (user_access($perm_any)) {
      // No filtering when 'any' type permission is given.
      return array();
    }
    else if (user_access($perm_own)) {
      return array('uid' => $account->uid);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Append conditions from the given array to the given $where and $args
   * elements.
   * 
   * This is a really internal method.
   */
  protected final function _conditionsAppend(&$where, &$args, &$conditions) {
    foreach ($conditions as $field => &$value) {
      // Fetch the associated field in Drupal schema API to check it is valid.
      // This will ensure we won't write an SQL request that does lookup on non
      // existing fields.
      // If the field does not exists, we are just going to drop it.
      if ($type = $this->_getFieldType($field)) {
        // Values can be an array, then we use the IN() statement, its in many
        // cases faster than using a lot of OR operators (also more convenient
        // to write for us).
        if (is_array($value)) {
          $where[] = "%s IN (" . db_placeholders($values, $type) . ")";
          // Pushes field name and values are query arguments.
          array_push($args, $field);
          foreach ($value as &$_value) {
            array_push($args, $_value);
          }
        }
        else {
          $where[] = "%s = " . $this->_getFieldPlaceholder($field);
          // Pushes field name and values are query arguments.
          array_push($args, $field);
          array_push($args, $value);
        }
      }
    }
  }

  /**
   * Load all internal implementation. This will allow other modules to use it
   * and alter query in order to add different or additional query filters.
   * 
   * @see XoxoFactory::loadAll()
   *   For parameters extended explanation.
   * 
   * @param int $pagerLimit = 0
   * @param boolean $excludeDisabled = TRUE
   * @param int $pagerElement = 0
   * @param array $conditions = array()
   *   (optional) Key/value pairs of parameters. Keys are field names, and
   *   values are field value to match in query.
   *   Set of additional parameters to append to the query. Notice this is not
   *   the same as db_* functions.
   *   You don't have to specify database placeholder, the method introspects
   *   schema API to find out which one to use. This is also a security check
   *   that will avoid to query non existing fields.
   *   If you are willing to create complex queries with join statements or
   *   anything else a bit more tricky, you should write your own full loadAll
   *   method.
   *   You can set here an arbitrary 'user_access', this will force this method
   *   to check current user access for objects. Allowed values are 'view',
   *   'update' or 'delete'.
   */
  protected final function _loadAll($pagerLimit = 0, $excludeDisabled = TRUE, $pagerElement = 0, $conditions = array()) {
    $query = "SELECT * FROM {%s}";
    $where = array();
    $args = array($this->getTableName());

    // Get a cache of what result returns, in for paging iteration.
    $all = FALSE;
    $fetched = array();

    // First aptempt by excluding already loaded objects.
    // We are not going to exclude from cache as soon as we have some filters
    // because we would not be able to restore cached elements that matches.
    if (!$pagerLimit && !empty($this->_loaded) && empty($conditions)) {
      $all = TRUE;
      // Exclude already loaded objects from query, this could help when working
      // with a lot of objets in the same PHP execution flow.
      foreach ($this->_loadAllExcludeFromCache() as $field => $values) {
        // Exclude empty exclusions arrays from query, and also check given
        // field exists in Drupal schema API.
        if (!empty($values) && ($type = $this->_getFieldType($field))) {
          $where[] = "%s NOT IN (" . db_placeholders($values, $type) . ")";
          // Pushes field name and values are query arguments.
          array_push($args, $field);
          foreach ($values as &$value) {
            array_push($args, $value);
          }
        }
      }
    }

    // Check for status exclusion.
    if ($excludeDisabled) {
      $where[] = "status <> %d";
      $args[] = XoxoObject::STATUS_DISABLED;
    }

    // Check for user access.
    if (isset($conditions['user_access'])) {
      // Fetch new fields associated conditions.  
      $access_conditions = $this->_getUserAccessConditions($value);
      if (!is_array($access_conditions)) {
        // If user has no rights at all, just return an empty array (nothing
        // to load here, it will, at least, avoid one SQL request).
        return array();
      }
      else {
        $this->_conditionsAppend($where, $args, $access_conditions);
      }
      // Remove it from array so the rest of the algorithm won't try to lookup
      // for 'user_access' within the schema API.
      unset($conditions['user_access']);
      unset($access_conditions);
    }

    // Append other condition parameters given.
    $this->_conditionsAppend($where, $args, $conditions);

    // Rebuild query.
    if (!empty($where)) {
      $query .= ' WHERE ' . implode(' AND ', $where);
    }

    // Do the real query.
    if ($pagerLimit) {
      // Ensure an order to avoid random pages content.
      // FIXME: This should be parametized, we'll see that later.
      $query .= " ORDER BY oid ASC";
      $result = pager_query($query, $pagerLimit, $pagerElement, NULL, $args);
    }
    else {
      $result = db_query($query, $args);
    }

    // Iterate over loaded object in order to load them all.
    while ($data = db_fetch_object($result)) {
      $object = $this->_populate($data);
      $this->_objectLoaded($object);
      // Append new result to fetched array. This will ensure we return only
      // the fetched elements.
      $fetched[$object->getIdentifier()] = $object;
    }

    // If no pager limit is given, the users wants the full database. This
    // seems quite dumb, but give it to him won't kill kill us, it will only
    // kill its site performances.
    if ($all) {
      $fetched = &$this->_loaded;
    }

    $objects = array();

    // New iteration over cache, to ensure we got all objects including those
    // we already loaded before.
    foreach ($fetched as &$object) {
      // Even if the query has already excluded disabled objects, they may have
      // been arbitrary loaded during the execution flow.
      if ($all && $excludeDisabled && $object->getStatus() == XoxoObject::STATUS_DISABLED) {
        continue;
      }

      // Proceed to load method fetching.
      switch ($this->_loadMethod) {
        case self::LOAD_METHOD_CLONE:
          $objects[] = clone($object);
          break;
        case self::LOAD_METHOD_INSTANCE:
        default:
          // We can afford a reference here, because when returning the array
          // it will be copied.
          $objects[] = $object;
      }
    }
    return $objects;
  }

  /**
   * Save a layout instance into database. This will not save included
   * content.
   * 
   * @param XoxoExportableObject $object
   * 
   * @throws XoxoFactoryException
   *   In case of invalid instance given.
   */
  public final function save(XoxoObject $object) {
    $insert = FALSE;

    // Get a clean object clone, and set data for database saving.
    $prepared = clone($object);
    $prepared->clean();
    $data = array(
      'data' => serialize($prepared),
      'uid' => $object->getOwnerUid(),
      'status' => $object->getStatus(),
    );

    // Try to get back current identifier, if none exists, then we are doing an
    // insert.
    try {
      $oid = $object->getIdentifier();
      $update = array('oid');
      $data['oid'] = $oid;
      $data['updated'] = time();
      // An updated object is always a non default.
      $data['defaults'] = 0;
      $ret = self::SAVED_UPDATED;
    }
    catch (OoxException $e) {
      $insert = TRUE;
      $update = array();
      $data['updated'] = $data['created'] = time();
      $data['defaults'] = $object->isDefault() ? 1 : 0;
      $ret = self::SAVED_NEW;
    }

    // Create a transaction (this won't work with MyISAM) to ensure we can set
    // the name property to the oid property.
    // We do this because when operating on XoxoObject, name has no sense
    // name must be used only for exportables that needs a unique machine name.
    // Note that in any case, there really a few chances that this transaction
    // overlaps with another, so, if you are lucky, it may still work using the
    // MyISAM MySQL engine.
    db_query("START TRANSACTION");

    // Last chance modification for overiding classes.
    $this->_preSave($object, $data);

    // Real save.
    if (drupal_write_record($this->getTableName(), $data, $update) == FALSE) {
      db_query("COMMIT");
      $ret = self::SAVED_ERROR;
    }
    else {
      // This should always goes here.
      if (empty($data['name'])) {
        db_query("UPDATE {%s} SET name = '%s' WHERE oid = %s", array($this->getTableName(), $data['oid'], $data['oid']));
      }
      $this->_postSave($object, $data);
      db_query("COMMIT");
      // Check for errors.
      if (db_error()) {
        $ret = self::SAVED_ERROR;
      }
      // If no error, and we are in the case of object creation, update cache.
      else if ($insert) {
        $object->setIdentifier($data['oid']);
        $this->_loaded[$data['oid']] = $object;
        $this->_objectLoaded($object);
      }
      else {
        // Reset default flag at update.
        $object->setDefault(FALSE);
        $this->_loaded[$data['oid']] = $object;
      }
    }

    return $ret;
  }

  /**
   * Last chance for overriding classes to alter the $data array when saving.
   * This is an internal method, please do not override it unless you really
   * know what you are doing.
   * 
   * This function is run within a database transaction, any request that
   * does an error here will then implies a database rollback and object
   * save failure.
   * 
   * @param XoxoObject $object
   *   Object being saved. If this is an insert, the object does not have
   *   its identifier set yet.
   * @param array &$data
   *   The data array that will be passed to drupal_write_record().
   */
  protected function _preSave(XoxoObject $object, &$data) { }

  /**
   * Save has been done, with no errors.
   * 
   * This function is run within a database transaction, any request that
   * does an error here will then implies a database rollback and object
   * save failure.
   * 
   * @param XoxoObject $object
   *   Object being saved. If this is an insert, the object does not have
   *   its identifier set yet.
   * @param array &$data
   *   The data array that got through to drupal_write_record(), identifier
   *   is set in this array as $data['oid'].
   */
  protected function _postSave(XoxoObject $object, &$data) {
    // Save all options.
    OptionsFactory::getInstance()->save($object);
  }

  /**
   * Delete object.
   * 
   * Using the identifier to delete an object is mandatory, because other
   * factories may want to delete other stuff arround. Delete has no error
   * handling, once the object has been deleted, there is no comming back.
   * 
   * @see XoxoFactory::_postDelete()
   * 
   * @param int $oid
   *   XoxoObject identifier.
   * @param boolean $clearCache = TRUE
   *   (optionaml) Internal parameter, when deleting a massive amount of
   *   objects, you should set this to TRUE then call yourself the
   *   XoxoFactory::_clearCache() method after the massive deletion.
   * 
   * @return int
   *   Number of rows deleted.
   */
  public final function deleteByIdentifier($oid, $clearCache = TRUE) {
    db_query("DELETE FROM {%s} WHERE oid = %d", $this->getTableName(), $oid);
    $deleted = db_affected_rows();
    if (isset($this->_loaded[$oid])) {
      // Clear local cache.
      unset($this->_loaded[$oid]);
    }
    // Even if no rows where deleted (which means the object does not exists
    // anymore) give a chance for other factories to delete garbage data that
    // could remain.
    $this->_postDelete($oid);
    // Force factory to fully clear its internal cache. Since we are deleting
    // objects using their identifier, we can not predict other attributes that
    // override classes could use for internal static caching.
    if ($deleted && $clearCache) {
      $this->_cacheClear();
    }
    return $deleted;
  }

  /**
   * Every factory that uses secondary database tables to store data arround
   * the objects should use this method to clean their data. Use this method
   * to also cleanup your caches.
   * 
   * Their is no transaction here and object has already been deleted, just
   * delete all remaining garbage.
   * 
   * @see XoxoFactory::deleteByIdentifier()
   * 
   * @param int $oid
   *   XoxoObject identifier.
   */
  protected function _postDelete($oid) { }

  /**
   * Delete objects using the given table field. This field must exist in
   * current table schema API cache.
   * 
   * @param string $field
   *   Table field name.
   * @param mixed $value
   *   Value, placeholder will be choosed using the schema API.
   * 
   * @return int
   *   Number of rows deleted.
   */
  public function deleteByField($field, $value) {
    $deleted = 0;
    if ($placeholder = $this->_getFieldPlaceholder($field)) {
      $result = db_query("SELECT oid FROM {%s} WHERE %s = " . $placeholder, array($this->getTableName(), $field, $value));
      while ($oid = db_result($result)) {
        $deleted += $this->deleteByIdentifier($oid, FALSE);
      }
      if ($deleted) {
        $this->_cacheClear();
      }
    }
    return $deleted;
  }

  /**
   * Delete objects owned by the given account.
   * 
   * @param int|object $account
   *   Account identifier or object.
   * 
   * @return int
   *   Number of rows deleted.
   */
  public function deleteByOwner($account) {
    $uid = is_object($account) ? $account ->uid : $account;
    return $this->deleteByField('uid', $uid);
  }

  /**
   * Check if the given identifier exists in database.
   * 
   * @param int $oid
   *   Identifier.
   * 
   * @return boolean
   */
  public function existsByIdentifier($oid) {
    return (bool) db_result(db_query("SELECT 1 FROM {%s} WHERE oid = %d", $this->getTableName(), $oid));
  }

  /**
   * Factory method used by common code. This should be overriden by any
   * specific factory implementation.
   * 
   * @param string $type = NULL
   *   (optional) Registry item type. If given, will spawn a factory named using
   *   the factory type as registry type and aptempt to create the given item
   *   type as class to instanciate the object.
   *
   * @return XoxoObject
   *   New empty instance.
   */
  public function createInstance($type = NULL) {
    $class = $this->_className;
    return new $class();
  }

  /**
   * Change only object status in database, leaving all other stuff unchanged.
   * Status must have already been changed in object.
   * 
   * @param XoxoObject $object
   */
  public function saveStatus(XoxoObject $object) {
    db_query("UPDATE {%s} SET status = %d WHERE oid = %d", array($this->getTableName(), $object->getStatus(), $object->getIdentifier()));
  }

  /**
   * Clear internal static cache, mostly used by unit testing.
   */
  public final function cacheClear() {
    $this->_cacheClear();
  }

  /**
   * Clear all internal cache.
   * You should reserve this function only for edge cases, avoid it if you can
   * manually refresh caches yourself.
   */
  protected function _cacheClear() {
    $this->_loaded = array();
  }

  /**
   * Get help text that will display on administration overview page.
   * 
   * FIXME: Quite ugly, find a better pattern than this.
   * 
   * @return string
   *   Human readable localized string.
   */
  public function getOverviewHelp() { return ""; }
}
