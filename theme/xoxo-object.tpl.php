<?php

/**
 * @file
 * Default display for XoXo objects.
 */
?>
<div class="<?php print implode(' ', $classes); ?>">
  <?php print $content; ?>
</div>
