<?php

/**
 * @file
 * XoXo theme and template preprocess functions.
 */

/**
 * Implementation of template_preprocess_TEMPLATE().
 */
function template_preprocess_xoxo_object(&$variables = array()) {
  $object = $variables['object'];
  // Check object is suitable for this theme function.
  if (!$object instanceof IRenderable) {
    $variables['object'] = '';
    $variables['classes'] = array();
    return;
  }
  $suggestions = array();
  // Avoid some PHP warnings.
  // All possible suggestions, this will allow great power to themer.
  if ($object instanceof IRegistrable && ($type = $object->getType())) {
    try {
      $suggestions[] = 'xoxo-object-' . $type . '-' . $object->getIdentifier();
    }
    catch (OoxException $e) { }
    if ($object instanceof XoxoExportableObject) {
      try {
        $suggestions[] = 'xoxo-object-' . $type . '-' . $object->getName();
      }
      catch (OoxException $e) { }
    }
    $suggestions[] = 'xoxo-object-' . $type;
  }
  $suggestions[] = 'xoxo-object';
  $variables['classes'] = $variables['template_files'] = $suggestions;
  // Content.
  if (!empty($variables['content'])) {
    // This is the particular case where the module calling the theme function
    // already rendered the object, and passes it through the content argument.
    // In this case, we assume this is the already rendered object and skip the
    // rendered phase. Don't mess with this parameter.
    // See the hook_theme() implementation for further details.
  }
  else if ($variables['preview']) {
    $variables['content'] = $object->renderPreview();
  }
  else {
    $variables['content'] = $object->render();
  }
}
