
/**
 * @file
 * XoXo administration pages customizations.
 */

(function ($) {

Drupal.behaviors.XoxoOverviewForm = function(context) {
  // Droppable behavior for toolbar.
  $('.xoxo-enabled:not(.xoxo-processed)', context).each(function() {
    var checkbox = this,
        parent = $(checkbox).parents('tr'),
        name = parent.attr('rel');
    if (name) {
      $(checkbox).change(function(e) {
        $(checkbox).attr({disabled: 'disabled'});
        $.ajax({
          method: 'GET',
          cache: false,
          async: true,
          url: Drupal.settings.Xoxo.ajaxPrefixUrl + '/' + name + '/toggle-status',
          success: function(data) {
            switch (data.status) {
              case 0:
                parent.removeClass('xoxo-object-row-enabled');
                parent.addClass('xoxo-object-row-disabled');
                break;
              case 1:
              default:
                parent.removeClass('xoxo-object-row-disabled');
                parent.addClass('xoxo-object-row-enabled');
                break;
            }
            $(checkbox).removeAttr('disabled');
          },
          error: function(data) {
            $(checkbox).removeAttr('disabled');
          },
          dataType: 'json'
        });
        e.stopPropagation();
        return false;
      });
    }
    $(this).addClass("xoxo-processed");
  });
};

})(jQuery);
