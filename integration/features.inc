<?php

/**
 * @file
 * XoXo Features module integration.
 * 
 * If you want this to work, also enable the ctools module. Because you are
 * probably building a large site, we are going to assume that ctools module
 * is already enabled on your installation.
 */

/**
 * Implementation of hook_features_api().
 */
function xoxo_features_api() {
  $ret = array();
  $ret['xoxo_object'] = array(
    'name' => t("@type objects"),
    'default_file' => FEATURES_DEFAULTS_INCLUDED,
    'features_source' => TRUE,
  );
  return $ret;
}

/**
 * Implementation of hook_features_export_options().
 */
function xoxo_object_features_export_options() {
  $options = array();
  foreach (oox_registry_get('factory')->getItemCache() as $type => $item) {
    if ($item['exportable']) {
      foreach (xoxo_factory_get($type)->loadAll() as $object) {
        try {
          $name = $object->getName();
          $options[$type . '--' . $name] = t("From @type : @name", array('@type' => t($item['name']), '@name' => $name));
        }
        catch (OoxException $e) {
          watchdog('xoxo', "An exception happened during hook_features_export_options() execution: " . nl2br($e->getMessage()));
        }
      }
    }
  }
  return $options;
}

/**
 * Implementation of hook_features_export().
 */
function xoxo_object_features_export($data, &$export, $module_name = '') {
  $pipe = array();
  foreach ($data as $identifier) {
    $export['features']['xoxo_object'][$identifier] = $identifier;
    list($type, $name) = explode('--', $identifier, 2);
    try {
      $factory = xoxo_factory_get($type);
      $object = $factory->loadByName($name);
      // Allow custom implementations to expose their own dependencies.
      $factory->featuresDependencies($object, $pipe, $export);
    }
    catch (OoxException $e) {
      watchdog('xoxo', "An exception happened during hook_features_export() execution: " . nl2br($e->getMessage()), NULL);
    }   
  }
  return $pipe;
}

/**
 * Implementation of hook_features_export_render().
 */
function xoxo_object_features_export_render($module = 'foo', $data) {
  $ret = $types = array();
  // First phase, build types array filled with objects.
  foreach ($data as $identifier) {
    $export['features']['xoxo_object'][$identifier] = $identifier;
    list($type, $name) = explode('--', $identifier, 2);
    if (!isset($types[$type])) {
      $types[$type] = array();
    }
    try {
      $factory = xoxo_factory_get($type);
      $types[$type][] = $factory->loadByName($name);
    }
    catch (OoxException $e) {
      watchdog('xoxo', "An exception happened during hook_features_export_render() execution: " . nl2br($e->getMessage()), NULL);
    }
  }
  // Second phase, build code for each objects lots.
  foreach ($types as $type => $objects) {
    try {
      $ret[$type . '_defaults'] = implode("\n", xoxo_factory_get($type)->exportMutiple($objects, TRUE, 2));
    }
    catch (OoxException $e) {
      watchdog('xoxo', "An exception happened during hook_features_export_render() execution: " . nl2br($e->getMessage()), NULL);
    }
  }
  return $ret; 
}
