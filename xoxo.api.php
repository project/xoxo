<?php

/**
 * @file
 * XoXo module hooks definition and documentation.
 */

/**
 * This hook allows modules to use the default oox module administration
 * UI to manage their exportables.
 * 
 * Implementing this hook will implicitely create a new hook called after your 
 * object type: hook_OBJECT_TYPE_defaults.
 * 
 * Implementing this hook using a custom factory will also implicitely create
 * the features module oox to allow you to import/export your modules.
 * In order for this to work, this is mandatory that you override the import
 * export methods in your custom factory.
 * 
 * @return array
 *   It must return a valid array, following this convention:
 *     - Each key is an exportable type name. This is the name you will get in
 *       the menu pathes. Value is an array filled with options.
 *   Following are array options:
 *     - 'name' : string
 *       Human readable type name, not localized.
 *     - 'administrable' : boolean
 *       (optional) default is FALSE
 *       TRUE or FALSE, will tell the oox module to build the full
 *       administration tools to manage object instances.
 *     - 'table' : string
 *       (required)
 *       This setting will tell the table name where exportable objets are
 *       stored. Note that this table must have a compatible schema with
 *       the 'oox_data' table.
 *       Whatever happens, oox module will try to create this table
 *       using the right schema.
 *     - 'load_method' : const
 *       (optional) default is OoxFactory::LOAD_METHOD_INSTANCE
 *       Two methods are right for this:
 *         - OoxFactory::LOAD_METHOD_INSTANCE
 *           Normal method, this will only copy the loaded object reference, as
 *           node module will do with node_load(). This means every modification
 *           on a loaded object will be reflected to wherever this object has
 *           been loaded.
 *         - OoxFactory::LOAD_METHOD_CLONE
 *           This method forces the load method to use the clone() function on
 *           static loaded instance each time you request it. This ensure that
 *           statically cached instances won't be modified (so any other module
 *           that asks to load an instance will have a fresh clean one).
 *     - 'class' : string
 *       (optional) default is 'OoxFactory'
 *       Because you might want to add some behaviors depending on your object
 *       type, you'll then need to override the OoxFactory by creating
 *       your own class that extends this one.
 *       You should always do that, for at least one good reason which is the
 *       OoxFactory::exportCode() and OoxFactory::importCode()
 *       functions are not implemented in default factory, and will throw an
 *       exception if you try to run them.
 *     - 'factory_file' : string
 *       (optional) default is NULL
 *       If your factory class PHP code is not included by your module, use this
 *       directive to tell the oox module to include the right PHP file
 *       when trying to load the factory.
 *       This can be either a module relative path, filename, or absolute full
 *       path.
 *     - 'exportable' : boolean
 *       (optional) default is FALSE
 *       Tells if the objects are exportable or not. If TRUE, the custom factory
 *       must implement the exportCode() and importCode() methods.
 *     - 'target' string
 *       (mandatory)
 *       This is the classname of objects your module exports. If you use this
 *       facilities you must define at least your own objects that override
 *       either OoxExportable or OoxObject classes.
 */
function hook_xoxo_factory() {
  $items = array();
  // Non exportable data type. Ideal for site data. No UI will be present, but
  // the factory will easily be able to handle a really large amount of them.
  // UI, defaults, import and export features will be disable for this type, you
  // will only have access to the factory using:
  // <code>
  //   $factory = oox_factory_get('my_object_type');
  // </code>
  $items['my_object_type'] = array(
    'name' => "My simple object type",
    'table' => 'mymodule_my_object_type',
    'target' => 'MyObjectType',
  );
  // Exportable data type without its own factory.
  // This object will be exportable using only the features module.
  $items['my_exportable_type'] = array(
    'name' => "My exportable type",
    'table' => 'mymodule_my_exportable_type',
    'target' => 'MyExportableType',
    'exportable' => TRUE,
  );
  // Exportable data type with its own factory.
  // It will exportable using the features module, but also manually through
  // the menu under Administer > Build > My Full Type. You will also be able
  // to manage objects throught this UI.
  $items['my_full_type'] = array(
    'name' => "My Full Type",
    'administrable' => TRUE,
    // Next one can be omited.
    'table' => 'mymodule_object_type',
    // This is the default, can be omited.
    'load_method' => XoxoFactory::LOAD_METHOD_INSTANCE,
    'class' => 'MyObjectTypeFactory',
    // Never leave the file empty, it is useless (it won't affect performances)
    // but just don't do it until you really need to specify a file.
    'file' => '',
    'target' => 'MyObjectType',
    'exportable' => FALSE,
  );
  return $items;
}

/**
 * Return default instances defined by a custom module. This will allow site
 * administrator to use some objects without using the database.
 * 
 * This will also be the implemented hook when using the features module in
 * order to export site configuration.
 * 
 * @return array
 *   Array of default XoxoExportableObject instances.
 */
function hook_OBJECT_TYPE_defaults() {
  $items = array();
  $items[] = new OoxExportable('one', "My first instance");
  $items[] = new OoxExportable('two', "My second instance");
  $items[] = new OoxExportable('three', "My third instance");
  return $items;
}
